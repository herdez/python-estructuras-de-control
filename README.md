## Indentación en Python

Por convención se usan cuatro espacios para indentar. La indentación es usada para delimitar bloques de código en estructuras de control, funciones, etc.

## Comentarios en Python

Un comentario es texto que sirve para documentar el código. Es una buena práctica de programación comentar siempre el código.

#### Comentario de una sola línea

Para comentar una sola línea usamos el signo `#` antes del texto.

```python
#Este es un comentario de una sola línea en Python
```

#### Docstrings

Los comentarios de varias líneas deben estar encerrados con `"""` o apóstrofes `'''`. Los `docstrings` son usados para generar documentación que es desplegada con la función help().

```python
"""Este es un comentario de varias líneas en Python. Este   
   docstring genera documentación muy útil mediante la función help()."""
```

Un ejemplo de uso del `docstring`:

```
>>> def greet():
...     """Ejemplo del uso de docstring"""
...     print("Hola, solamente probando el docstring")
... 
>>> help(greet)

Help on function greet in module __main__:

greet()
    Ejemplo del uso de docstring

```

Para mayor información sobre `docstrings` visita el siguiente link: [PEP 257 -- Docstring Conventions](https://www.python.org/dev/peps/pep-0257/).

## Estructuras de control en Python

Python tiene las siguientes estructuras de control:

- Estructuras de control condicionales: `if`, `else`, `elif`.
- Estructuras de control iterativas: `while`, `for`.


## Estructuras de control condicionales

Alteran el funcionamiento de un procedimiento de acuerdo a una condición.

#### Estructuras de control `if`, `else`, `elif` 

Estas estructuras revisan que una condición se cumpla para ejecutar una pieza de código.

```
if <conditional>:
    #piece of code
elif <conditional>:
    #piece of code
else:
    #piece of code
```

```python
a = 9
b = 3
if a == b:
    print("a y b son iguales")
else:
    print("a y b son diferentes")
```

## Estructuras de control iterativas

Esta estructura permite ejecutar un bloque de código una cantidad específica de veces.

#### Estructura de control `for`

La estructura `for` en Python es usada para iterar de forma dinámica un objeto iterable. 

```
for <variable> in <object>:
    #piece of code
```

`for` puede iterar sobre los elementos de cualquier secuencia de una lista o string en el orden que aparecen en tal secuencia.

```
>>> for number in numbers:
...     print("El número es: ", str(number))
... 
El número es:  2
El número es:  3
El número es:  5
El número es:  7
```

#### Función `range()`

La función `range` genera una progresión aritmética si es necesario iterar sobre una secuencia de números.

```
"""Genera secuencia de 0 hasta el 9 en incrementos de 1"""
>>> for number in range(10):
...     print(number)
... 
0
1
2
3
4
5
6
7
8
9

```
```
"""Genera secuencia de 1 hasta el 10 en incrementos de 1"""
>>> for number in range(1, 11):
...     print(number)
... 
1
2
3
4
5
6
7
8
9
10
```
```
"""Genera secuencia de 1 hasta el 10 en incrementos de 2"""
>>> for number in range(1, 11, 2):
...     print(number)
... 
1
3
5
7
9
```

Con el uso de la función `range` también podemos generar una lista de valores iterables:

```
>>> list(range(7))
[0, 1, 2, 3, 4, 5, 6]
```

#### Estructura de control `while`

Esta estructura ejecuta un bloque de código mientras se cumpla una condición determinada.

```
while <condition>:
    #piece of code
```

```
>>> floor = 1
>>> 
>>> while floor <= 10:
...   print("El piso es: ", str(floor))
...   floor += 1
... 
El piso es:  1
El piso es:  2
El piso es:  3
El piso es:  4
El piso es:  5
El piso es:  6
El piso es:  7
El piso es:  8
El piso es:  9
El piso es:  10
```

#### Palabras reservadas `break`, `continue`, `else` y `exit()`

La palabra `continue` permite continuar con la siguiente iteración de un ciclo (loop).

La palabra reservada `break` termina la ejecución de un bloque de código. Se aplica también a funciones, métodos y cualquier elemento que contenga bloques de código ejecutables.

```
"""Ejemplo de 'continue' y `break' usando la estructura 'while'"""

>>> floor = 1
>>> stop  = 8
>>> 
>>> while floor <= 10:
...     if floor <= stop:
...        print("Elevador subiendo")
...        floor += 1
...        continue
...     print("El piso es: ", str(floor))
...     break
... 
Elevador subiendo
Elevador subiendo
Elevador subiendo
Elevador subiendo
Elevador subiendo
Elevador subiendo
Elevador subiendo
Elevador subiendo
El piso es:  9

```

```
"""Ejemplo de 'continue' usando la estructura 'for'"""

>>> number = 4
>>> 
>>> for n in range(1, 10):
...     if n == number:
...         print("Your number is: ", str(n))
...         continue
...     print("Number is: ", str(n))
... 
Number is:  1
Number is:  2
Number is:  3
Your number is:  4
Number is:  5
Number is:  6
Number is:  7
Number is:  8
Number is:  9
```

La palabra reservada `else` se ejecuta cuando el ciclo termina por haber recorrido la lista (con la estructura `for`) o cuando la condición se vuelve falsa (con la estructura `while`).

```
"""Ejemplo de `break` y `else` usando la estructura `for`"""

>>> number = 11
>>> 
>>> for n in range(1, 10):
...     if n == number:
...         print("I found your number: ", str(n))
...         break
... else:
...     print("I didn't find your number")
... 
I didn't find your number

```

La función `exit()` termina la ejecución de un programa y cierra el interprete de python.

```
"""Ejemplo de 'exit()' usando la estructura 'for'

>>> number = 4
>>> 
>>> for n in range(1, 10):
...     if n == number:
...         print("I found your number: ", str(n))
...         exit()
... else:
...     print("I didn't find your number")
... 
I found your number:  4


```

## Estructura de control `switch-case` en Python

En Python no existe la estructura condicional `switch` o `case`. En Python podemos usar diccionarios `dict` para generar este tipo de estructura condicional, no es necesario anidar varias estructuras `if-elif-else`.

Por ejemplo en Ruby tendríamos la siguiente estructura `case`:

```ruby
grade = 7
case grade
when 5
  p "You are Out"
when 6
  p "You are almost Out"
when 7
  p "You are regular"
when 8
  p "You are good"
when 9
  p "You are very good"
when 10
  p "You are excelent"
else
  p "Not Graduated"
end

#=> "You are regular" 
```

En Python podríamos hacer uso de un método y un diccionario `dict` para generar este tipo de estructura condicional:

```python
"""Este caso no permite agregar el valor por default 'else'"""

def grades_to_string(grade):
    switch = {
        5: "You are Out",
        6: "You are almost Out",
        7: "You are regular",
        8: "You are good",
        9: "You are very good",
        10: "You are excelent"
    }
    
    """llamamos el caso del número recibido como argumento,
       sin agregar un valor por default"""
    value = switch[grade]
    print(value)

#Cuando es el caso de la calificación 7
grades_to_string(7)
#>>You are regular

#Cuando es el caso de alguna calificación no encontrada
grades_to_string(4)
#>>>...KeyError: 4

```

Podemos contemplar un caso por default `else` haciendo uso de un método nativo de Python:

```python
"""Este caso permite agregar el valor por default 'else'"""

def grades_to_string(grade):
    switch = {
        5: "You are Out",
        6: "You are almost Out",
        7: "You are regular",
        8: "You are good",
        9: "You are very good",
        10: "You are excelent"
    }
    
    """llamamos el caso del número recibido como argumento,
       agregando un valor por default en caso de no encontrarse"""
    value = switch.get(grade, "Not Graduated")
    print(value)

#Cuando es el caso de una calificación no encontrada
grades_to_string(4)
#>>>Not Graduated

```

## Ejercicio - Identifica even or odd

Para este ejercicio es importante documentarse acerca de como insertar valores en una lista.

Define la función `even_odd` que identifique números pares e impares. Las pruebas `driver code` deben ser igual a `True`.

```python
"""even_odd function"""

#driver code
print(even_odd([3, 1, 5, 6, 8, 10, 23, 25, 23]) == ['odd', 'odd', 'odd', 'even', 'even', 'even', 'odd', 'odd', 'odd'])
print(even_odd([12, -4, -23, 0]) == ['even', 'even', 'odd', 'even'])

```